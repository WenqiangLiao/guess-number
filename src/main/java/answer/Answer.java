package answer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class Answer {
    
    private static final int ANSWER_LENGTH = 4;
    private static final int ANSWER_NUMBER_LIMIT = 10;
    private List<Integer> answer = null;
    private Object IOException;

    public Answer(Path filePath) {
        try {
            List<Integer> numbers = getAnswerFromFile(filePath);
            validAnswer(numbers);
            this.setAnswer(numbers);
        } catch (Exception e) {
            this.setAnswer(generateRandomAnswer());
        }
    }
    
    public List<Integer> getAnswer() {
        return this.answer;
    }
    
    public void setAnswer(List<Integer> answer){
        this.answer = answer;
    }
    
    @Override
    public String toString() {
        return answer.stream().map(String::valueOf).collect(Collectors.joining(""));
    }
    
    private List<Integer> getAnswerFromFile(Path filePath) throws IOException {
        // Need to be implemented
//        if(filePath == null){
//            throw new IOException();
//        }else {
//            BufferedReader reader = Files.newBufferedReader(filePath, Charset.forName("UTF-8"));
//            List<String> strList = Arrays.asList(reader.readLine());
//            List<Integer> intList = strList.stream().map(Integer::parseInt).collect(Collectors.toList());
//            return intList;
//        }
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(String.valueOf(filePath));
        BufferedReader reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(inputStream)));
        String number = reader.readLine();
        return number.chars().mapToObj(Character::getNumericValue).collect(Collectors.toList());
    }
    
    private List<Integer> generateRandomAnswer() {
        // Need to be implemented
        List<Integer> randomAnswer = new ArrayList<>();
        randomAnswer.add(new Random().nextInt(9));
        randomAnswer.add(new Random().nextInt(9));
        randomAnswer.add(new Random().nextInt(9));
        randomAnswer.add(new Random().nextInt(9));
        return randomAnswer;
    }
    
    static void validAnswer(List<Integer> answer) throws InvalidAnswerException {
        // Need to be implemented
        List<Integer> newAnswer = answer.stream().distinct().collect(Collectors.toList());
        if(newAnswer.size() < 4){
            throw new InvalidAnswerException("input Wrong input, input again");
        }
    }
}
