package answer;

public class InvalidAnswerException extends Exception {
    public InvalidAnswerException(String message) {
        super(message);
    }

    public InvalidAnswerException() {
//        System.out.println("input Wrong input, input again");
    }
}
