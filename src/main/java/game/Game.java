package game;

import answer.Answer;
import com.sun.org.apache.bcel.internal.generic.SWITCH;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Game {
    
    private Answer answer;
    private Map<String, String> guessResult;
    
    public Game() {
        Path filePath = Paths.get("answer.txt");
        this.answer = new Answer(filePath);
        this.guessResult = new LinkedHashMap<>();
    }
    
    public String guess(List<Integer> numbers) {
        String result = "";
        if (numbers.equals(answer.getAnswer())) {
            result = "4A0B";
        } else {
            int countA = getASize(numbers);
            int countB = getBSize(numbers);
            result = countA + "A" + countB + "B";
        }
        String key = numbers.stream().map(String::valueOf).collect(Collectors.joining(""));
        this.guessResult.put(key, result);
        return result;
    }
    
    public boolean isOver() {
        // Need to be implemented
        return guessResult.size() == 6 || guessResult.values().contains("4A0B");
    }
    
    public String getResult() {
        // Need to be implemented
        GuessResult guessResult = new GuessResult(this.guessResult);
        GameResult gameResult = guessResult.getGameResult();
        String result = "";
        switch (gameResult){
            case WIN:
                result = guessResult.getResult() + "\n" +"Congratulations, you win!";
                break;
            case LOST:
                result = guessResult.getResult() + "\n" + "Unfortunately, you have no chance, the answer is " + answer.toString() + "!";
                break;
            default:
                result = guessResult.getResult();
        }

        return result;
    }
    
    private int getBSize(List<Integer> numbers) {
        // Need to be implemented
        int numberB = 0;
        for (Integer i:numbers){
            if (this.answer.getAnswer().contains(i)){
                numberB += 1;
            }
        }
        return numberB - getASize(numbers);

    }
    
    private int getASize(List<Integer> numbers) {
        // Need to be implemented
        int numberA = 0;
        for (int i=0; i < numbers.size(); i++){
            if (numbers.get(i) == this.answer.getAnswer().get(i)){
                numberA += 1;
            }
        }
        return numberA;
    }
}
