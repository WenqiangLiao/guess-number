package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GuessResult {
    private Map<String, String> guessRecord;
    private static final int CHANCE_LIMIT = 6;

    private static final String NORMAL = "NORMAL";
    private static final String WIN = "WIN";
    private static final String LOST = "LOST";

    public GuessResult(Map<String, String> guessRecord) {
        this.guessRecord = guessRecord;
    }

    public String getResult() {
        // Need to be implemented
//        Integer length = this.guessRecord.size();
        List<String> resultList = new ArrayList<>();
        for (Map.Entry<String, String> entry : this.guessRecord.entrySet()){
            resultList.add(entry.getKey() + " " + entry.getValue() + "\n");
        }
        return resultList.stream().collect(Collectors.joining()).trim();
    }
    
    public GameResult getGameResult() {
        // Need to be implemented
        if (this.guessRecord.values().contains("4A0B")){
            return GameResult.WIN;
        }
        if (this.guessRecord.size() >= CHANCE_LIMIT){
            return GameResult.LOST;
        } else {
            return GameResult.NORMAL;
        }
    }
}
